score = input('Please input a score between 0.0 and 1.0: ')
score = float(score)

try:
    def computegrade():
        if (score > 1.0): 
          print("You may be a genius, but this test has a limit, follow instructions")
          exit()
    
        elif (score >= 0.9):
          print("A")
 
        elif (score >= 0.8):
          print ("B")

        elif (score >= 0.7):
          print ("C")
 
        elif (score >= 0.6):
          print ("D")

        elif (score < 0.0):
          print("Excuse me, but how did you get a negative score?")

except:
   print("Only numerical values between 1.0 and 0.1 please: ")
   exit()

computegrade()

     
  
