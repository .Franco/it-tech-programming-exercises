repeat_lyrics

def print_lyrics():
    print("I'm a lumberjack, and I'm okay")
    print("I sleep all night and I work all day")

def repeat_lyrics():
    print_lyrics()
    print_lyrics()

#The error is "NameError: name 'repeat_lyrics' is not defined".
#This happens because the program is run line by line
#The program can't be asked to be run a line that hasn't been defined just yet
