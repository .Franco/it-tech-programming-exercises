Exercise 9: How do you fix a "Syntax Error"?

A Syntax Error means that the program doesn't understand what you're saying. Check your spelling, and check that you're following the language's 'grammar'