import re
name = input('Enter a file name: ')
try:
	handle = open(name)
except:
	print (name, 'Not found')
	exit()
numlist = []
for line in handle:
	line = line.rstrip()
	num = re.findall('^New .*: ([0-9]+)', line)
	if len(num) > 0:
		for number in num:
			number = float(number)
		numlist.append(number)
mean = (sum(numlist) / len(numlist))

print('The average number is',mean)
