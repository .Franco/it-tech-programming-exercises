name = input('Gib file pls: ')
count = dict()
try:
    handle = open(name)

except:
    print('File not found:' , name)
    exit()

for line in handle:
   word = line.split()

   if len(word) < 2 or word[0] != 'From':
       continue
   
   else:
       if word[1] not in count:
           count[word[1]] = 1
       else:
           count[word[1]] += 1


most = None

for key in count:
    if most is None or count[key] > most:
        most = count[key]
        
print('The email that sent the most is', key,'at', most, 'emails')

