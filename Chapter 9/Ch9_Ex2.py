days = dict()
name = input('Feed me a file name: ')

try:
    fhand = open(name)

except:
    print('File not found: ', name)
    exit()

for line in fhand:
    count = line.split()
    if len(count) < 3 or count[0]  != 'From':
        continue


    else:
        if count[2] not in days:
            days[count[2]] = 1

        else:
            days[count[2]] = days[count[2]] + 1

print(days)
    
